const fullName = document.getElementById('fullName');
const email = document.getElementById('email');
const message= document.getElementById('message');
const error = document.getElementById('error-message');
const form = document.getElementById('form');

form.addEventListener("submit", function(e) {

    if (fullName.value === '' || email.value === '' || message.value === '') {
        e.preventDefault();
        error.textContent ='Please make sure to fill your name, email and a brief message';     
    }else{
        error.textContent =''; 
    }

})

window.addEventListener("scroll", function() {
    var header = document.querySelector("header");
    header.classList.toggle("sticky", window.scrollY > 0);
  });

  function toggleMenu(){
    var menuToggle = document.querySelector('.toggle');
    var menu = document.querySelector('.menu');
    menuToggle.classList.toggle('active');
    menu.classList.toggle('active');
  }
